
#include "ex1.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>   /* pour rand */
#include <string.h>

static Personne peoples[20];
static int people_in_peoples = 0;


int compare_date(Date date1,Date date2){
    if (date1.years > date2.years){
        return 1;
    }
    if (date1.years < date2.years){
        return 0;
    }
    /*si meme years => on continue */
    if (date1.month > date2.month){
        return 1;
    }
    if (date1.month < date2.month){
        return 0;
    }
    /*si meme day => on continue */
    if (date1.day > date2.day){
        return 1;
    }
    if (date1.day < date2.day){
        return 0;
    }
    return 0;
}

void tri_bulle_person_by_age(){
    int i=0;
    Personne tmp;
    int changement =1;
    while (changement ==1){
        changement = 0;
        for(i=1 ; i < people_in_peoples ; i++){
            if(compare_date(peoples[i].date,peoples[i-1].date) > 0){
                tmp = peoples[i];
                peoples[i] = peoples[i - 1];
                peoples[i - 1] = tmp;
                changement = 1;

            }
        }
    }
}


char * scanf_char(char * msg,int size_buffer,char * buffer){
    int retour_erreur = 0;
    printf("%s\n",msg);
    retour_erreur = scanf("%s", buffer);

    if (retour_erreur != 1){
        printf("Il y a eu une erreur dans la saisie %d \n",retour_erreur);
        exit(0);
    }
}

int scanf_int(char * msg,int min,int max){
    int number = -10;
    int retour_erreur = 0;
    printf("%s\n",msg);
    printf("INFO l entier rentrée doit être en %d et %d \n:",min,max);

    retour_erreur = scanf("%d", &number);
    if (retour_erreur != 1){
        printf("Il y a eu une erreur dans la saisie %d \n",retour_erreur);
        exit(0);
    }
    if(number < min || number > max){
        printf("Erreur l entier rentrée devait être en %d et %d or il est : %d \n",min,max,number);
        exit(0);
    }

    return number;
}
void print_date(Date date){
    printf("%d/%d/%d \n",date.day,date.month,date.years);
}


void print_person(Personne person){
    printf("Nom %s\n",person.name);
    printf("Prenom %s\n",person.firstname);
    printf("L'age est : \n");
    print_date(person.date);
}

void print_table_person(){
    int i;
    printf("Nombre de personnes %d \n",people_in_peoples);
    for(i=0 ; i < people_in_peoples; i++){
        print_person(peoples[i]);
    }
}


Personne create_person(){

    struct  Personne person;

    scanf_char("Entrer votre nom",20,person.name);
    scanf_char("Entrer votre prenom",20,person.firstname);

    int age_day = scanf_int("Entrer le jour de votre naissance : ",0,31);
    int age_month = scanf_int("Entrer le mois de votre naissance : ",0,12);
    int age_years = scanf_int("Entrer l annee de votre naissance : ",1900,2050);
    printf("Votre age %d/%d/%d \n",age_day,age_month,age_years);

    struct Date date;
    date.day = age_day;
    date.month = age_month;
    date.years = age_years;

    person.date = date;
    return person;

}

void edit_person(Personne* person_edit){
    char* name = scanf_char("Editer le nom",20,person_edit->name );
    char* firstname = scanf_char("Editer le prenom",20,person_edit->firstname);

    int age_day = scanf_int("Editer le jour de votre naissance : ",0,31);
    int age_month = scanf_int("Editer le mois de votre naissance : ",0,12);
    int age_years = scanf_int("Editer l'annee de votre naissance : ",1900,2050);

    Date date_edit;
    date_edit.day = age_day;
    date_edit.month = age_month;
    date_edit.years = age_years;

    person_edit->date = date_edit;
}

void print_search_person(){
    printf("\n\n\n");
    print_table_person();
    char name_search[20] ;
    scanf_char("Entre le nom de la personne voulue",20,name_search);
    int i =0;
    int choix;
    int exist = 0;
    for(i ; i < people_in_peoples ; i++) {
        if (strcmp(peoples[i].name,name_search) == 0) {
            exist = 1;
            printf("la comparaison entre %s et %s vaut %d \n",peoples[i].name,name_search,strcmp(peoples[i].name,name_search));

            printf("\n\nLa personne selectionnée est %d fege %d\n",i,strcmp(peoples[i].name,name_search));
            print_person(peoples[i]);
            printf("Action disponibles \n");
            printf("1) Modifier \n");
            printf("2) Afficher \n");
            printf("3) Supprimer \n");

            choix = scanf_int("Saisir votre choix",0,3);

            switch (choix){
                case 1:
                    edit_person(&peoples[i]);
                    break;
                case 2:
                    print_person(peoples[i]);
                    break;
                case 3:
                    remove_people(i);
                    printf("\nAprès supression, le nouveau tableau est : \n");
                    print_table_person();
                    break;
            }
        }
    }
    if (exist == 0 ){
        printf("la personne %s n existe pas \n",name_search);
    }
}

void remove_people(int index){
    for(index ; index < people_in_peoples ; index++) {
		print_person(peoples[index]);
        peoples[index] = peoples[index+1];
    }
    people_in_peoples --;
}

Personne generate_person(){
    Personne person;
    scanf_char("nom :",20,person.name);
    strncpy(person.firstname,"test_prenom",11);

    Date date;
    date.day = 10 + rand()%20;
    date.month = 3;
    date.years = 2000;

    person.date = date;
    return person;

}


int main(){

    peoples[0] = create_person();
    people_in_peoples = people_in_peoples +1;

    peoples[1] = create_person();
    people_in_peoples = people_in_peoples +1;

    peoples[2] = create_person();
    people_in_peoples = people_in_peoples +1;
    
    /*
    people_in_peoples = people_in_peoples + 3;
    peoples[0] = generate_person();
    peoples[1] = generate_person();
    peoples[2] = generate_person();
    */

    print_person(peoples[0]);
    printf("Le tab avant tri\n");
    print_table_person(peoples,people_in_peoples);

    tri_bulle_person_by_age(peoples,people_in_peoples);
    printf("Le tab apres tri \n");

    print_table_person(peoples,people_in_peoples);

    while(1){
        print_search_person();
    }
    return 0;

}
