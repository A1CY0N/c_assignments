
#define N 5

#include <stdio.h>
#include <stdlib.h>
#include <time.h>   /* pour rand */
#include <string.h>


typedef struct maillon {
	int x;
	struct maillon * suiv;
} maillon;

int scanf_int(char * msg,int min,int max){
    int number = -10;
    int retour_erreur = 0;
    printf("%s\n",msg);
    printf("INFO l entier rentrée doit être en %d et %d \n:",min,max);

    retour_erreur = scanf("%d", &number);
    if (retour_erreur != 1){
        printf("Il y a eu une erreur dans la saisie %d \n",retour_erreur);
        exit(0);
    }
    if(number < min || number > max){
        printf("Erreur l entier rentrée devait être en %d et %d or il est : %d \n",min,max,number);
        exit(0);
    }

    return number;
}

void remove_maillon(maillon * tete,int maillon_number){
	maillon *lc = tete;
	int cpt = 0;
	while(lc->suiv != NULL){
		if (cpt == maillon_number){
			lc->suiv  = lc->suiv->suiv;
			return;
		}
		cpt ++;
	}
}


void print_linked_list(maillon * tete){
	maillon *lc = tete;
	int cpt = 0;
	while(lc->suiv != NULL){
		printf("%d ) Valeur du champs courant = %d \n",cpt,lc->x);
		lc = lc->suiv;
		cpt ++;
	}
	
}

int main(void)
{
	int linked_list_size = N;
	maillon *lc;
	maillon *tete;
	int cpt;
	/*init des maillons/*/
	lc = (maillon *) malloc(sizeof(maillon));
	tete = lc;
	/* Creation des maillons en fin de liste */
	for(cpt=1;cpt<N;cpt++) /* Pour tous les maillons à créer*/
	{
		lc->suiv = (maillon *) malloc(sizeof(maillon));
		lc = lc->suiv;
	}
	lc->suiv = NULL;
	cpt = 0;
	lc = tete;
	/* remplissage des valeurs de chacun des maillons*/
	while (lc != NULL) /*tant que le maillon courant n'est pas le suivant du dernier maillon de la liste*/
	{
		lc->x = cpt; /* affectation*/
		cpt++; /* increment du compteur*/
		lc = lc->suiv;/* passe au suivant*/
	}
	lc = tete;
	while (lc != NULL) /* tant que le maillon courant n'est pas le suivant du dernier maillon de la liste*/
	{
		printf("Valeur du champs courant = %d \n",lc->x);
		printf("Adresse maillon courant= %p et du suivant %p\n",lc, lc->suiv);
		lc = lc->suiv;/* passe au suivant*/
	}
	
	int number;
	while(1){
		print_linked_list(tete);
		number = scanf_int("entrer le numero du maillons voulu",0,linked_list_size - 1);
		remove_maillon(tete,number);
	}
	
	
	return 0;
}
