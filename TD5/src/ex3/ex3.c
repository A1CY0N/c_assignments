/*
*   TP 5: Pointeurs
*
*   insuffisance des appels de fonction
*   avec passage par valeur
*/
#include <stdio.h>
void echangerPointeurs(int **ppa, int **ppb)
{
int *tmp;

tmp = *ppa;
*ppa = *ppb;
*ppb = tmp;
}
int main()
{
	int *x, *y;
	int x2 =12, y2= 34;
	x = &x2;
	y = &y2;
	printf("Avant échange : x = %d ; y = %d\n",*x, *y);
	echangerPointeurs(&x,&y);
	printf("Après échange : x = %d ; y = %d\n",*x, *y);
	return 0;
}
