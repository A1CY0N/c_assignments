
#include "ex1.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>   /* pour rand */
#include <string.h>


/*
*   TP 5: Pointeurs
*
*   : lecture de l'adresse des éléments d'un tableau
*/


/*#include <stdio.h>
#define N 5
void main(void)
{
	/*
	int i;
	float t[N] = {10.6, 20.3, 30.6, 40.6, 50.5};
	for(i=0;i<N;i++)
	printf("t[%d] = %d; &t[%d] = %u\n",i,t[i],i,t[i]);
	* 
	* Nous observons que "&" permet de voir le contenu d'un pointeur, et que
	* le tableau est constitué de plusieurs entiers qui occupent chacun 4 unités
	* (octets ?)
	*/
	
	
	/*int *px;
	px = (int *) malloc (sizeof(int));
	*px = 1;
	printf("Adresse de px : %X\n", &px);
	printf("Adresse pointee par le pointeur px = %X\n", px);
	printf("Valeur memorisee a l'adresse pointee par le pointeur px = %d\n\n", *px);
	
	
	 /* Adresse de px : 10918618
	 * Adresse pointee par le pointeur px = 184E010
	 * Valeur memorisee a l'adresse pointee par le pointeur px = 1
	 */
/*}*/

