#include <stdio.h>
#include <stdlib.h>
#include <time.h>   /* pour rand */
#include <string.h>

int compare_r_int(int a, int b){
    if (a== b){
        return 1;
    }
    else{
        return 0;
    }
}

char compare_r_letter(int a, int b){
    if (a== b){
        return 'V';
    }
    else{
        return 'F';
    }
}


int compare_r_int_float(float a, float b){
    if (a== b){
        return 1;
    }
    else{
        return 0;
    }
}

char compare_r_letter_float(float a, float b){
    if (a== b){
        return 'V';
    }
    else{
        return 'F';
    }
}

int main(){
    printf("5 et 5, %d\n", compare_r_int(5,5));
    printf("5 et 6, %c\n", compare_r_letter(5,6));
    printf("5.0 et 6.3, %d\n", compare_r_int_float(5.0,6.3));
    printf("5.2 et 5.2, %c\n", compare_r_letter_float(5.2,5.2));


    return 0;

}