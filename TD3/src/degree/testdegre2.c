#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "degre.c"

int lireChoix(){
    printf("0)Sortir\n");
    printf("1)CelsiusAKelvin\n");
    printf("2)CelsiusAFahrenheit\n");
    printf("3)KelvinACelsius\n");
    printf("4)KelvinAFahrenheit\n");
    printf("5)FahrenheitACelsius\n");
    printf("6)FahrenheitAKelvin\n");
    int choix = -1;
    int retour_erreur = 0;
    while(choix< 0 || choix>6) {
        printf("Saisir votre choix: \n");
        retour_erreur = scanf("%d", &choix);
        if (choix == 0 ){
            printf("Exit\n");
            exit(0);
        }
    }
    if (retour_erreur != 1){
        printf("il y a eu une erreur dans la saisie %d \n",retour_erreur);
        exit(0);
    }
    return choix;
}

void executerChoix(int choix){
    int result;
    int temperature;
    int retour_erreur = 0;
    printf("Saisir votre temperature: \n");
    retour_erreur = scanf("%d", &temperature);

    if (retour_erreur != 1){
        printf("il y a eu une erreur dans la saisie %d \n",retour_erreur);
        exit(0);
    }

    switch (choix){
        case 1:
            result = CelsiusAKelvin(temperature);
            break;
        case 2:
            result = CelsiusAFahrenheit(temperature);
            break;
        case 3:
            result = KelvinACelsius(temperature);
            break;
        case 4:
            result = KelvinAFahrenheit(temperature);
            break;
        case 5:
            result = FahrenheitACelsius(temperature);
            break;
        case 6:
            result = FahrenheitAKelvin(temperature);
            break;
    }
    printf("le resultat est %d \n",result);
}

int main(){
    int choix = lireChoix();
    executerChoix(choix);
    return 0;
}

