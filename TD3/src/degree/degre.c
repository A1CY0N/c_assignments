#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "degre.h"

int CelsiusAKelvin(int number){
    return number + 273;
}
int CelsiusAFahrenheit(int number){
    return (9/5) * number + 32;
}
int KelvinACelsius(int number){
    return number - 273;
}
int KelvinAFahrenheit(int number){
    return (KelvinACelsius(number) * (9/5)) + 32;
}
int FahrenheitACelsius(int number){
    return (number - 32) / (9/5);
}
int FahrenheitAKelvin(int number){
    return FahrenheitACelsius(number) + 273;
}







