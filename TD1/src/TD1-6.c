#include <stdio.h>
#define PI 3.14159

int main(int argc, char * argv[]){
	float rayon;
	printf("entrer le rayon:");
	scanf("%f", &rayon);
	printf("Le rayon est %f: \n",rayon);
	printf("Perimetre = %f \n", 2*PI*rayon);
	printf("Surface = %f \n", PI*rayon*rayon);
	return 0;
}
