#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159

int main(int argc, char * argv[]){
	char operation ='n';
	float number1;
	float number2;
	float result;
	while(operation != '/' && operation != '*' && operation !='+' && operation != '-' && operation !='s'){
		printf("entrer l'operation : \n");
		scanf("%c", &operation);
	}
	if (operation == 's'){
		printf(" vous avez demander à sortir");
		exit(0);
	}

	printf("entrer le nombre 1: \n");
	scanf("%f", &number1);

	printf("entrer le nombre 2: \n");
	scanf("%f", &number2);


	if (operation == '*'){
		result = number1 * number2;
	}

	if (operation == '/'){
                if (number2 == 0){
			printf("erreur pour une division par 0");
			exit(1);
		}
		result = number1 / number2;
	}
	
	if (operation == '+'){
		result = number1 + number2;
	}

	if (operation == '-'){
		result = number1 - number2;
	}

	
	printf("Le resultat  est %f: \n",result);
	return 0;
}
