#include <stdio.h>

int main(int argc, char * argv[]){
	float xFloat = 10/3;
	printf(" x floattant %f\n",xFloat);
	int xInt = 10/3;
	printf(" x int %d\n", xInt);
	float xFloat2 = 10.0/3;
	printf("xFloat2 =  %f\n", xFloat2);
	float xFloat3 = 10.0/3.0;
	printf("xFloat3 = %f\n", xFloat3);
	float xFloat4 = (float) 10/3;
	printf("xFloat4 = %f\n", xFloat4);
	float xFloat5 = ((float) 10)/3;
	printf("xFloat5 = %f\n", xFloat5);
	float xFloat6 = ((float) 10)/3;
	printf("xFloat6 = %.1f, xFloat6 = %.6f\n", xFloat6, xFloat6);

return 0;
}
