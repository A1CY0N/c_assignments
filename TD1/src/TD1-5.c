#include <stdio.h>

int main(int argc, char * argv[]){
	printf(" 5&6 ne marche pas car & est pour le pointeur \n");
        printf(" 5&&6 %d  \n",5&&6);
        printf(" 5|6 => ou binaire %d  \n",5|6);
        printf(" 5||6  => ou logique %d  \n",5||6);
        printf(" 5^6 xor binaire %d  \n",5^6);
        printf(" 5^^6 impossible");
        printf(" ~5 not binaire %d  \n",~5);
        printf(" !5 not logique %d  \n",!5);

	return 0;
}
