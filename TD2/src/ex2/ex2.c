#include <stdio.h>
#include <stdlib.h>
#include <time.h>   /* pour rand */


void tri_minima(int* tab, int sizeTab){
    int indexMin=0;
    int i=0;
    int j=0;
    int tmp =0;
    for(i=0; i<sizeTab; i++){
        for(j=i; j<sizeTab;j++){
            if(tab[j] < tab[indexMin]){
                indexMin = j;
            }
        }
        tmp = tab[i];
        tab[i] = tab[indexMin];
        tab[indexMin] = tmp;

    }

}


void tri_insertion(int* tab, int sizeTab){
    int i,j,item;
    for (i = 1; i < sizeTab; i++){
        item =  tab[i];
        j = i-1;

        while (j >= 0 && tab[j] > item){
            tab[j+1] = tab[j];
            j = j-1;
        }
        tab[j+1] = item;
    }
}


void tri_bulle(int* tab, int sizeTab){
    int i=0;
    int tmp;
    int changement =1;
    while (changement ==1){
        changement = 0;
        for(i=1; i<sizeTab; i++){
            if(tab[i] < tab[i-1]){
                tmp = tab[i];
                tab[i] = tab[i-1];
                tab[i-1] = tmp;
                changement = 1;
            }
        }
    }
}

void print_tab(int* tab, int sizeTab){
    int i =0;
    for(i = 0;i<sizeTab;i++){
        printf("%d,", tab[i]);
    }
    printf("\n");
}


int main()
{

    int sizeTab = 30;
    int i =0;
    int *tab = malloc(50 * sizeof(int));
    for(i = 0;i<sizeTab;i++){
        tab[i]= rand()%51 +1;
    }
    printf("tableau avant tri_bulle :\n");
    print_tab(tab, sizeTab);
    tri_bulle(tab, sizeTab);
    printf("tableau apres tri_bulle :\n");

    print_tab(tab, sizeTab);

    for(i = 0;i<sizeTab;i++){
        tab[i]= rand()%51;
    }
    printf("tableau avant tri_minima :\n");
    print_tab(tab, sizeTab);
    tri_minima(tab, sizeTab);
    printf("tableau apres tri_minima :\n");
    print_tab(tab, sizeTab);


    for(i = 0;i<sizeTab;i++){
        tab[i]= rand()%51;
    }
    printf("tableau avant tri_insertion :\n");
    print_tab(tab, sizeTab);
    tri_insertion(tab, sizeTab);
    printf("tableau apres tri_insertion :\n");

    print_tab(tab, sizeTab);

    return 0;

}
