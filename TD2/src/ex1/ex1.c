#include <stdio.h>
#include <stdlib.h>




char* carre(int size){
    char * retour = malloc((size * size + size) * sizeof(char));
    int i=0;
    int j = 0;
    int k =0;

    for (i=0;i<size;i++){

        for (j=0;j<size;j++){
            retour[k] = '*';
            k++;
        }
        retour[k] = '\n';
        k++;

    }
    return retour;
}

char* triangle1(int size){
    char * retour = malloc(((size * 2 - 1) * (size+1)) * sizeof(char));
    int hauteur =0;
    int largeur = 0;
    int k =0;

    for (hauteur=0;hauteur<size;hauteur++){
        for (largeur=0;largeur<=size+1;largeur++){
            if (largeur< hauteur){
                retour[k] = '*';
                k++;
            }
            else{
                retour[k] = '#';
                k++;
            }

        }
        retour[k] = '\n';
        k++;
    }

    for (;hauteur>0;hauteur--){
        for (largeur=0;largeur<=size+1;largeur++){
            if (largeur< hauteur){
                retour[k] = '*';
                k++;
            }
            else{
                retour[k] = '#';
                k++;
            }

        }
        retour[k] = '\n';
        k++;
    }


    return retour;
}


char* triangle2(int size){
    char * retour = malloc(((size * 2 - 1) * (size+1)) * sizeof(char));
    int hauteur =0;
    int largeur = 0;
    int k =0;

    for (hauteur=0;hauteur<size;hauteur++){
        for (largeur=0;largeur<=size;largeur++){
            if (largeur <= (size - hauteur)){
                retour[k] = '#';
                k++;
            }
            else{
                retour[k] = '*';
                k++;
            }

        }
        retour[k] = '\n';
        k++;
    }

    for (;hauteur>0;hauteur--){
        for (largeur=0;largeur<size;largeur++){
            if (largeur >= (size - hauteur)){
                retour[k] = '*';
                k++;
            }
            else{
                retour[k] = '#';
                k++;
            }

        }
        retour[k] = '\n';
        k++;
    }


    return retour;
}

char* pyramide(int size){
    char * retour = malloc(((size * 2 - 1) * (size)) * sizeof(char));
    int hauteur =0;
    int largeur = 0;
    int k =0;

    for(hauteur=0;hauteur<=size;hauteur++){
        for(largeur=0; largeur<(size-hauteur); largeur ++){
            retour[k] = '#';
            k++;
        }
        for(largeur=1; largeur<(hauteur*2); largeur ++){
            retour[k] = '*';
            k++;
        }
        for(largeur=0; largeur<(size-hauteur); largeur ++){
            retour[k] = '#';
            k++;
        }
        retour[k] = '\n';
        k++;
    }

    return retour;
}


char* losange(int size){
    char * retour = malloc(2*((size * 2 - 1) * (size)) * sizeof(char));
    int hauteur =0;
    int largeur = 0;
    int k =0;

    for(hauteur=0;hauteur<=size;hauteur++){
        for(largeur=0; largeur<(size-hauteur); largeur ++){
            retour[k] = '#';
            k++;
        }
        for(largeur=1; largeur<(hauteur*2); largeur ++){
            retour[k] = '*';
            k++;
        }
        for(largeur=0; largeur<(size-hauteur); largeur ++){
            retour[k] = '#';
            k++;
        }
        retour[k] = '\n';
        k++;
    }

    for(hauteur=1;hauteur<=size;hauteur++){
        for(largeur=0; largeur<hauteur; largeur ++){
            retour[k] = '#';
            k++;
        }
        for(largeur=1; largeur<2*size-(hauteur*2); largeur ++){
            retour[k] = '*';
            k++;
        }
       for(largeur=0; largeur<hauteur; largeur ++){
            retour[k] = '#';
            k++;
        }
        retour[k] = '\n';
        k++;
    }

    return retour;
}



int main()
{


	int n;
	int saisieIsOk = 0;
	int choix = 0;
	while (saisieIsOk == 0){
		printf("entrer un nombre pour la taille de la figure: \n");
        scanf("%d", &n);
        if (1<=n && n<=20){
            saisieIsOk = 1;
        }
        else{
            printf("le nombre doit etre entre 1 et 20");
        }
	}


	printf(" Vos choix:\n");
	printf("1 - Figure en carré \n");
	printf("2 - Figure en triangle 1 \n");
	printf("3 - Figure en triangle 2 \n");
	printf("4 - Figure en pyramide \n");
	printf("5 - Figure en losange \n");
	saisieIsOk = 0;

	while (saisieIsOk == 0){
		printf("entrer votre choix: \n");
        scanf("%d", &choix);
        if (1<=choix && choix<=5){
            saisieIsOk = 1;
        }
        else{
            printf("le nombre doit etre entre 1 et 4\n");
            	printf(" Vos choix:\n");
                printf("1 - Figure en carré \n");
                printf("2 - Figure en triangle 1 \n");
                printf("3 - Figure en triangle 2 \n");
                printf("4 - Figure en pyramide \n");
                printf("5 - Figure en losange \n");
        }
	}
	switch(choix) {
        case 1:
            printf(carre(n));
            break;
        case 2:
            printf(triangle1(n));
            break;
        case 3:
            printf(triangle2(n));
            break;
        case 4:
            printf(pyramide(n));
            break;
        case 5:
            printf(losange(n));
            break;
	}


    return 0;
}


